# encoding: utf-8
"""
@author: anna
@contact: a.l.d.latour@liacs.leidenuniv.nl
@time: 2/16/21 10:11 AM
@file: test-example.py
@desc:
"""


def add(a, b):
    return a + b


def test_add():
    assert add(2, 3) == 5
    assert add('space', 'ship') == 'spaceship'


def subtract(a, b):
    return a + b  # <--- fix this in step 8


# uncomment the following test in step 5
def test_subtract():
   assert subtract(2, 3) == -1